import * as firebase from 'firebase';
import '@firebase/auth';
import Keys from './Keys';

var firebaseConfig = {
  apiKey: Keys.API_KEY,
  authDomain: Keys.AUTH_DOMAIN,
  projectId: Keys.PROJECT_ID,
  storageBucket: Keys.STORAGE_BUCKET,
  messagingSenderId: Keys.MESSAGING_SENDER_ID,
  appId: Keys.APP_ID,
  measurementId: Keys.MEASUREMENT_ID,
  databaseURL: Keys.DATABASE_URL,
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;