export default class Colors {
    //#region WHITE SHADES
    static WHITE = '#FFFFFF'
    static LIGHT_WHITE = '#EDF6F960'
    //#endregion

    //#region BLACK SHADES
    static BLACK = '#000000'
    static BACKGROUND_COLOR = '#0B0A0A'
    static FOOTER_COLOR = '#1C1616'
    static SEARCHBAR_COLOR = '#1C1616'
    static INPUT_BACKGROUND_COLOR = '#1C1616'
    static BACKGROUND_BUTTON_COLOR = '#312D2D'
    //#endregion

    //#region GREEN SHADES
    static BUTTON_COLOR = '#9CCC96'
    //#endregion

    //#region RED SHADES
    static LOGOUT_BUTTON_COLOR = '#D40000'
    //#endregion
}