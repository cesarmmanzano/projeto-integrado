import React, { useState } from 'react'
import { View, Image, Text, TouchableOpacity, TextInput, SafeAreaView } from 'react-native'
import Colors from '../styles/Colors'
import GenericStyles from '../styles/GenericStyles'
import CustomStyles from '../styles/CustomStyles'
import firebase from '../../BackEnd/config/firebase'
import StringConstants from '../../Commons/StringConstants'
import PageTokens from '../../Commons/PageTokens'
import Utils from '../../Commons/Utils'

export default Login = ({ navigation }) => {

    //#region STATES
    const [areButtonsDisabled, setAreButtonsDisabled] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    //#endregion

    //#region METHODS
    function validateSignInInfos() {
        if (email === '' || password === '') {
            Utils.showFlashMessage(StringConstants.MISSING_APP_FILL_ALL_FIELDS)
        } else {
            signInWithEmailAndPassword()
        }
    }

    function signInWithEmailAndPassword() {
        setAreButtonsDisabled(true)
        firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .catch(error => {
                Utils.showFlashMessage(StringConstants.MISSING_APP_USER_PASSWORD_INVALID)
                console.error(error);
                setAreButtonsDisabled(false)
            });
    }
    //#endregion

    return (
        <SafeAreaView style={GenericStyles.baseMainPageContainerStyle}>

            <View style={CustomStyles.registerImageContainerStyle}>
                <Image source={require('../../../assets/img/logo.png')} style={CustomStyles.registerImageStyle} />
                <Text style={CustomStyles.registerTextStyle}>Missing</Text>
            </View>

            <View style={GenericStyles.mainEmailAndPasswordContainerStyle}>
                <View style={GenericStyles.baseInputStyle}>
                    <TextInput placeholder={StringConstants.MISSING_APP_EMAIL_PLACEHOLDER}
                        placeholderTextColor={Colors.LIGHT_WHITE}
                        value={email}
                        style={[GenericStyles.baseTextStyle, CustomStyles.registerInputStyle]}
                        onChangeText={(value) => setEmail(value)} />
                </View>
                <View style={GenericStyles.baseInputStyle}>
                    <TextInput placeholder={StringConstants.MISSING_APP_PASSWORD_PLACEHOLDER}
                        placeholderTextColor={Colors.LIGHT_WHITE}
                        value={password}
                        secureTextEntry={true}
                        style={[GenericStyles.baseTextStyle, CustomStyles.registerInputStyle]}
                        onChangeText={(value) => setPassword(value)} />
                </View>
                <TouchableOpacity activeOpacity={0.8} onPress={validateSignInInfos} disabled={areButtonsDisabled}>
                    <View style={[GenericStyles.baseButtonStyle, { opacity: areButtonsDisabled ? 0.5 : 1 }]}>
                        <Text style={GenericStyles.baseButtonTextStyle}>Entrar</Text>
                    </View>
                </TouchableOpacity>
            </View>

            <View style={CustomStyles.loginSeparatorStyle} />

            <View style={CustomStyles.loginTextContainerStyle}>
                <Text style={[GenericStyles.baseTextStyle, { opacity: 0.6 }]}>Não tem conta? </Text>
                <TouchableOpacity activeOpacity={0.6} onPress={() => navigation.navigate(PageTokens.REGISTER)} disabled={areButtonsDisabled}>
                    <Text style={[GenericStyles.baseTextStyle, { fontFamily: StringConstants.MISSING_APP_OPEN_SANS_BOLD_FONT, opacity: areButtonsDisabled ? 0.5 : 1 }]}>Registre-se</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView >
    )
}