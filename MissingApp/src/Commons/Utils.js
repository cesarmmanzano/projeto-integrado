import { Dimensions } from "react-native"
import { showMessage } from "react-native-flash-message"
import Colors from '../FrontEnd/styles/Colors'
import StringConstants from './StringConstants'
import firebase from 'firebase'
import * as ImagePicker from 'expo-image-picker'

export default class Utils {

    static getScreenWidth() {
        return Dimensions.get("window").width
    }

    static getScreenHeight() {
        return Dimensions.get("window").height
    }

    static isSmallScreen() {
        return this.getScreenHeight() < 700 || this.getScreenWidth() < 400
    }

    static getCurrentAge(date) {
        let splitDate = date.split("/")
        let newDate = new Date(splitDate[2], (splitDate[1] - 1), splitDate[0])
        let diff = Date.now() - newDate.getTime()
        let age = new Date(diff)
        return Math.abs(age.getUTCFullYear() - 1970)
    }

    static getImageFileUniqueName(path) {
        let randomNumber = Math.random()
        let fileEtensionNumber = Date.now() + randomNumber
        let filePath = path + "image" + fileEtensionNumber.toString()
        return filePath
    }

    static showFlashMessage(flashMessage) {
        showMessage({
            message: flashMessage,
            type: "info",
            floating: true,
            autoHide: true,
            hideOnPress: true,
            duration: 4000,
            style: {
                backgroundColor: Colors.BUTTON_COLOR,
                marginBottom: 5,
            },
            titleStyle: {
                color: Colors.WHITE,
                fontFamily: StringConstants.MISSING_APP_OPEN_SANS_SEMI_BOLD_FONT,
                fontSize: 15,
            },
        });
    }

    static isValidDate(currentBirthDate, currentDisappearanceDate) {
        let birthDateSplit = currentBirthDate.split("/")
        let disappearanceDateSplit = currentDisappearanceDate.split("/")
        let currentDate1 = new Date()
        let currentDate = new Date(currentDate1.getFullYear(), currentDate1.getMonth() + 1, currentDate1.getDate()).getTime()
        let birthDate = new Date(birthDateSplit[2], birthDateSplit[1], birthDateSplit[0]).getTime()
        let disappearanceDate = new Date(disappearanceDateSplit[2], disappearanceDateSplit[1], disappearanceDateSplit[0]).getTime()

        if (birthDate > disappearanceDate) {
            this.showFlashMessage(StringConstants.MISSING_APP_USER_INVALID_DATE_1)
            return false
        }

        if (disappearanceDate > currentDate) {
            this.showFlashMessage(StringConstants.MISSING_APP_USER_INVALID_DATE_3)
            return false
        }

        return true
    }

    static getCurrentUserFirebaseUid() {
        return firebase.auth().currentUser.uid
    }

    static async openImagePickerAsync() {
        return await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            aspect: [4, 3],
            quality: 1,
        });
    }

    static Cities = ["Campinas", "Paulinia", "Valinhos", "Vinhedo", "Indaiatuba", "Serra Negra"]
    static Genders = ["Masculino", "Feminino", "Outro"]
    static GendersValues = ["M", "F", "O"]
}